var contactButton = document.getElementById("contactButton");
var contactForm = document.getElementById("contactForm");

contactButton.addEventListener("click", function() {
    contactButton.style.display = "none";
    contactForm.style.display = "block";
});

document.getElementById("contactFormContent").addEventListener("submit", function(event) {
    event.preventDefault();

    var contact = "ceyciesaints@gmail.com";
    var subject = document.getElementById("contactSubject").value;
    var body = document.getElementById("contactBody").value;

    var mailtoLink = "mailto:" + contact + "?subject=" + subject + "&body=" + body;

    window.location.href = mailtoLink;
});
